import keyboard_dareu_ek810 from './assets/images/ban-phim/ek810/keyboard_dareu_ek810.png';
import keyboard_corsair_k70 from './assets/images/ban-phim/k70/keyboard_corsair_K70.jpg';
import keyboard_vortex from './assets/images/ban-phim/vortex/keyboard_vortex.png';

import case_galaxy from './assets/images/case/galaxy/case_galaxy.jpg';
import case_kenoo_pink from './assets/images/case/kenoo_pink/case_kenoo_pink.jpg';
import case_mik_white from './assets/images/case/mik_white/case_mik_white.png';

import mouse_asus_tufM3 from './assets/images/chuot/tuf_m3/mouse_asus_tufM3.png'
import mouse_fuhlen_g90 from './assets/images/chuot/fuhlen_g90/mouse_fulhen_g90.jpg'
import mouse_wireless_elecom_red from './assets/images/chuot/elecom_red/mouse_wireless_elecom_red.jpg'

import monitor_acer_24inch from './assets/images/man-hinh/acer_24/monitor_acer_24inch.jpg'
import monitor_acer_21 from './assets/images/man-hinh/acer_21/monitor_acer_21.5.png'
import monitor_asus_23 from './assets/images/man-hinh/asus_23/monitor_asus_23.8.png'

import ssd_normal_256 from './assets/images/ssd/ssd-sata-256/ssd_normal_256.jpg'
import ssd_nvme_256 from './assets/images/ssd/ssd-nvme-gyga-256/ssd_nvme_256.png'
import ssd_nvme_kingston_500gb from './assets/images/ssd/ssd-nvme-kingston-256/ssd_nvme_kingston_500gb.jpg'

import phone_dareu_eh416 from './assets/images/tai-nghe/dareu-eh416/phone_dareu_eh416.png'
import phone_dareu_eh722x from './assets/images/tai-nghe/dareu-eh722/phone_dareu_eh722x.jpg'
import phone_razer_wireless from './assets/images/tai-nghe/razer-wl/phone_razer_wireless.jpg'

import vga_amd_radeon_6600_8gb from './assets/images/vga/radeon_6600_8/vga_amd_radeon_6600_8gb.png'
import vga_geforce_1650 from './assets/images/vga/geforce_1650/vga_geforce_1650.png'
import vga_gigabyte_4gb_ddr6 from './assets/images/vga/gyga_4/vga_gigabyte_4gb_ddr6.jpg'
import Logo from './assets/logo/logo.png'
export {
    keyboard_dareu_ek810,
    keyboard_corsair_k70,
    keyboard_vortex,
    case_galaxy,
    case_kenoo_pink,
    case_mik_white,
    mouse_asus_tufM3,
    mouse_fuhlen_g90,
    mouse_wireless_elecom_red,
    monitor_acer_24inch,
    monitor_acer_21,
    monitor_asus_23,
    ssd_normal_256,
    ssd_nvme_256,
    ssd_nvme_kingston_500gb,
    phone_dareu_eh416,
    phone_dareu_eh722x,
    phone_razer_wireless,
    vga_amd_radeon_6600_8gb,
    vga_geforce_1650,
    vga_gigabyte_4gb_ddr6,
    Logo
}